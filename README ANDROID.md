
# PITO Android Coding Guide
This guide serves to outline the coding conventions, best practices and style for all PutItOut Android projects. The guide should always be adhered to, even in the case of small proof of concepts or prototype applications.

## Aim
The aim of this guide is to standardise the way in which Android application is written, which subsequently reduces friction in reading it, making it more maintainable and therefore of a better quality.

The guide does not aim to define an applications architecture or any implementation details, these are domain specific problems which should be driven by the software architects and developers.

## Resources
* [Android Studio Overview](http://developer.android.com/tools/studio/index.html)
* [Code Style Guidelines for Contributors](https://source.android.com/source/code-style.html)
* [Google Java Style](https://google-styleguide.googlecode.com/svn/trunk/javaguide.html)
* [Launch Checklist](http://developer.android.com/distribute/tools/launch-checklist.html)

## Table of Contents
* [Order Import Statements](#order-import-statements)
* [Fully Qualify Imports](#fully-qualify-imports)
* [Number per Line](#number-per-line)
* [Limit Line Length](#limit-line-length)
* [Class and Interface Declarations](#class-and-interface-declarations)
* [Use Spaces for Indentation](#use-spaces-for-indentation)
* [Follow Field Naming Conventions](#follow-field-naming-conventions)
* [Use Standard Brace Style](#use-standard-brace-style)
* [Comment Formats](#comment-formats)
* [Exceptions Blocks](#exceptions-blocks)
* [Limit Variable Scope](#limit-variable-scope)
* [Treat Acronyms as Words](#treat-acronyms-as-words)
* [Use TODO Comments](#use-todo-comments)
* [Be Consistent](#be-consistent)
* [Write Short Methods](#write-short-methods)
* [Define Fields in Standard Places](#define-fields-in-standard-places)
* [Use Javadoc Standard Comments](#use-javadoc-standard-comments)
* [Log Sparingly](#log-sparingly)
* [Java Packages and Class Names](#java-packages-and-class-names)
* [Resource Names](#resource-names)
* [Localizing](#localizing)
* [Dimen Resources](#dimen-resources)
* [Git](#git)
* [Unit Tests](#unit-tests)
* [UIAutomation Tests](#uiautomation-tests)
* [Follow Test Method Naming Conventions](#follow-test-method-naming-conventions)
* [Secure Coding Standard](#secure-coding-standard)
* [Best Practices](#best-practices)
* [Future Revisions](#future-revisions)

## Order Import Statements
The ordering of import statements is:
* Android imports
* Imports from third parties
* Java imports

## Fully Qualify Imports
When you want to use class Bar from package foo,there are two possible ways to import it:
```
import foo.*;
```
Pros: Potentially reduces the number of import statements.
```
import foo.Bar;
```
Pros: Makes it obvious what classes are actually used. Makes code more readable for maintainers. 

## Number per Line
One declaration per line is recommended since it encourages commenting. 
```
int level; // indentation level
int size; // size of table
```

## Limit Line Length
Each line of text in your code should be at most 100 characters long.

## Class and Interface Declarations
When coding Java classes and interfaces, the following formatting rules should be followed:

* No space between a method name and the parenthesis “(“ starting its parameter list
* Open brace “{” appears at the end of the same line as the declaration statement
* Methods are separated by a blank line
* Closing brace “}” starts a line by itself indented to match its corresponding opening statement, except when it is a null statement the “}” should appear immediately after the “{

## Use Spaces for Indentation
We use 4 space indents for blocks. We never use tabs. When in doubt, be consistent with code around you.
We use 8 space indents for line wraps, including function calls and assignments. For example, this is correct:
```
Instrument i =
        someLongExpression(that, wouldNotFit, on, one, line);
```

and this is not correct:
```
Instrument i =
    someLongExpression(that, wouldNotFit, on, one, line);
```

## Follow Field Naming Conventions
* Non-public, non-static field names start with m.
* Static field names start with s.
* Other fields start with a lower case letter.
* Public static final fields (constants) are ALL_CAPS_WITH_UNDERSCORES.

For example:
```
public class MyClass {
    public static final int SOME_CONSTANT = 42;
    public int publicField;
    private static MyClass sSingleton;
    int mPackagePrivate;
    private int mPrivate;
    protected int mProtected;
}
```

## Use Standard Brace Style
Braces do not go on their own line; they go on the same line as the code before them. So:
```
class MyClass {
    int func() {
        if (something) {
            // ...
        } else if (somethingElse) {
            // ...
        } else {
            // ...
        }
    }
}
```

We require braces around the statements for a conditional. Except, if the entire conditional (the condition and the body) fit on one line, you may (but are not obligated to) put it all on one line. That is, this is legal:
```
if (condition) {
    body(); 
}
```
and this is legal:

```
if (condition) body();
```
but this is still illegal:
```
if (condition)
    body();  // bad!

```

## Comment Formats
* Block comments are used to provide descriptions of files, methods, data structures and 
algorithms.
* Short comments can appear on a single line indented to the level of the code that follows.
* Very short comments can appear on the same line as the code 
they describe, but should be 
shifted far enough to separate them from the statements.
* The //comment delimiter can comment out a complete line or only a partial line. It shouldn't be used on consecutive multiple lines for text comments; however, it can be used in consecutive multiple lines for commenting out sections of code

## Exceptions Blocks
* Don't Ignore Exceptions
* Don't Catch Generic Exception

## Limit Variable Scope
The scope of local variables should be kept to a minimum. By doing so, you increase the readability and maintainability of your code and reduce the likelihood of error. Each variable should be declared in the innermost block that encloses all uses of the variable.

Local variables should be declared at the point they are first used. Nearly every local variable declaration should contain an initializer. If you don't yet have enough information to initialize a variable sensibly, you should postpone the declaration until you do.

One exception to this rule concerns try-catch statements. If a variable is initialized with the return value of a method that throws a checked exception, it must be initialized inside a try block. If the value must be used outside of the try block, then it must be declared before the try block, where it cannot yet be sensibly initialized:

```
// Instantiate class cl, which represents some sort of Set 
Set set = null;
try {
    set = (Set) cl.newInstance();
} catch(IllegalAccessException e) {
    throw new IllegalArgumentException(cl + " not accessible");
} catch(InstantiationException e) {
    throw new IllegalArgumentException(cl + " not instantiable");
}

// Exercise the set 
set.addAll(Arrays.asList(args));
```
Loop variables should be declared in the for statement itself unless there is a compelling reason to do otherwise:
```
for (int i = 0; i < n; i++) {
    doSomething(i);
}
```
and
```
for (Iterator i = c.iterator(); i.hasNext(); ) {
    doSomethingElse(i.next());
}
```

## Treat Acronyms as Words
Treat acronyms and abbreviations as words in naming variables, methods, and classes. The names are much more readable:

Good
```
XmlHttpRequest
getCustomerId
class Html
String url
long id
```

Bad
```
XMLHTTPRequest
getCustomerID
class HTML
String URL
long ID
```

Both the JDK and the Android code bases are very inconsistent with regards to acronyms, therefore, it is virtually impossible to be consistent with the code around you. Bite the bullet, and treat acronyms as words.

## Use TODO Comments
Use TODO comments for code that is temporary, a short-term solution, or good-enough but not perfect.
TODOs should include the string TODO in all caps, followed by a colon:
```
// TODO: Remove this code after the UrlTable2 has been checked in.
```
and
```
// TODO: Change this to use a flag instead of a constant.
```
If your TODO is of the form "At a future date do something" make sure that you either include a very specific date ("Fix by June 2015") or a very specific event ("Remove this code after all production mixers understand protocol V7.").

## Be Consistent
Our parting thought: BE CONSISTENT. If you're editing code, take a few minutes to look at the code around you and determine its style. If they use spaces around their if clauses, you should too. If their comments have little boxes of stars around them, make your comments have little boxes of stars around them too.

The point of having style guidelines is to have a common vocabulary of coding, so people can concentrate on what you're saying, rather than on how you're saying it. We present global style rules here so people know the vocabulary. But local style is also important. If code you add to a a file looks drastically different from the existing code around it, it throws readers out of their rhythm when they go to read it. Try to avoid this.

## Write Short Methods
To the extent that it is feasible, methods should be kept small and focused. It is, however, recognized that long methods are sometimes appropriate, so no hard limit is placed on method length. If a method exceeds 40 lines or so, think about whether it can be broken up without harming the structure of the program.

## Define Fields in Standard Places
Fields should be defined either at the top of the file, or immediately before the methods that use them.

## Use Javadoc Standard Comments
Every file should have a copyright statement at the top. Then a package statement and import statements should follow, each block separated by a blank line. And then there is the class or interface declaration. In the Javadoc comments, describe what the class or interface does.

## Log Sparingly
While logging is necessary, it has a significantly negative impact on performance and quickly loses its usefulness if it's not kept reasonably terse. The logging facilities provides five different levels of logging: 

* ERROR
* WARNING
* INFORMATIVE
* DEBUG
* VERBOSE
 
## Java Packages and Class Names
An android App should generally follow the following package structure
```
com.putitout.product
        .activities: All Activities with the word Activity pre-fixed by the Activity name: [Name]Activity e.g. MainActivity
        .adapters: All Adapters with the word Adapter pre-fixed by the Adapter name: [Name]Adapter e.g. UserListAdapter
        .services: All Services including API clients and other persistence related services e.g. UserService
        .components: All reusable components utilized in Activity and Fragments e.g. UserProfileComponent
        .dialogs: All Fragment Dialogs with the word Dialog pre-fixed by the dialog name: [Name]Dialog e.g. DeleteAccountConfirmationDialog
        .fragments: All Fragments with the word Fragment pre-fixed by the Fragment name: [Name]Fragment e.g. UserMapLocationFragment
        .utils: All cross package utilities with the word Utils pre-fixed by the Utility name: [Name]Utils e.g. StringUtils
```

## Resource Names
The following structure should be followed when naming resources.

group _ type _ name _ [state] _ [suffix]
- Group: Application area or screen. If the resource is used in different parts of applications 'common' should be used instead. e.g. actionbar, menu, media, popup, footer, audio, etc.
- Type: Resource Type. e.g. background, icon, button, textfield, list, menuitem, radiobutton, checkbox, tab, dialog, title, etc.
- Name: Descriptive name as to what the resource is about. e.g. play, stop.
- State: (Optional): The optional state of a parent resource. e.g. A button could be in 'normal', 'pressed', 'disabled' and 'selected'. A checkbox could be 'on' or 'off'. These resources should NEVER be used directly in layout but rather as state selectors.
- Suffix: (Optional): An arbitrary suffix that helps to further identify a property of the resource. e.g. bright, dark, opaque, layer.

Below are some examples of properly named resources.
```
common_background_app
audio_icon_play_on
common_icon_preferences
actionbar_button_send (XML resource)
	- action_button_send_normal
   	- action_button_send_pressed
   	- action_button_send_disabled
```

## Localizing
It is good practice to use the Android resource framework to separate the localized aspects of your application as much as possible from the core Java functionality. You can put most or all of the contents of your application's user interface into resource files. String resources placed in xml resources files such as strings.xml, config.xml.

## Dimen Resources
Dimens resources placed in dimen.xml. The following structure should be followed when naming dimentions.

property _ default _ group _ type _ name
```
property: Type of property reference. e.g. font_size, padding, margin, height, width.
default (Optional): Write "default" if is a general dimen.
group (Optional): Application area or screen. e.g. action_bar, menu, popup, wizard.
type (Optional): Type of resource. e.g. button, title, text, edittext.
name (Optional): Only if is necessary.
```
Below are some examples.
```
padding_default
font_size_action_bar_button
height_default_action_bar
```
We should have some dimension in all projects by default. These are:
```
padding_default
margin_default
font_size_default_button
font_size_default_title
font_size_default_text
height_default_action_bar
font_size_default_action_bar
```

## Git
**Branch Naming Convention**

Branch name should describes about developer and git issue number.
```
John Smith is going to starts working on git issue # 21 then a new feature branch name should be js-issue-21
```

**Commit Messages**

Now you have written some code in your branch, and are ready to commit. You want to make sure to write good, clean commit message. As important as the content of the change, is the content of the commit message describing it. A commit message should be short and comprehensive.

**Use Pull Requests**

Pull requests (PR) are an excellent tool for fostering code review and a good practice is for someone else to merge your code into the mainline, ensuring 2 sets of eyeballs review each feature.

**Avoiding code conflicts** 

Always make sure your feature branch should be up-to date from the latest develop.

## Unit Tests
We should write one unit test even though it is not requirement from client. You write one unit test to verify that login outlets are connected to code.

## UIAutomation Tests
One simple UIAutomation test should be written for example user registration for forgot password flow.

## Follow Test Method Naming Conventions
When naming test methods, you can use an underscore to seperate what is being tested from the specific case being tested. This style makes it easier to see exactly what cases are being tested.

For example:
```
testMethod_specificCase1 testMethod_specificCase2
void testIsDistinguishable_protanopia() {
    ColorMatcher colorMatcher = new ColorMatcher(PROTANOPIA)
    assertFalse(colorMatcher.isDistinguishable(Color.RED, Color.BLACK))
    assertTrue(colorMatcher.isDistinguishable(Color.X, Color.Y))
}
```

## Secure Coding Standard
* Do not store sensitive information on external storage (SD card) unless encrypted first
* Limit the accessibility of an app's sensitive content provider
* Do not allow WebView to access sensitive local resource through file scheme
* Do not broadcast sensitive information using an implicit intent
* Do not log sensitive information
* Do not grant URI permissions on implicit intents
* Ensure that sensitive data is kept secure
* Do not trust data that is world writable
* Do not provide addJavascriptInterface method access in a WebView which could contain untrusted content. (API level JELLY_BEAN or below)
* Check that a calling app has appropriate permissions before responding
* Consider privacy concerns when using Geolocation API
* Do not use the Android cryptographic security provider encryption default for AES
* Do not use the default behavior in a cryptographic library if it does not use recommended practices
* Properly verify server certificate on SSL/TLS
* Always pass explicit intents to a PendingIntent
* Do not cache sensitive information
* Do not bundle OAuth security-related protocol logic or sensitive data into a relying party's app
* To request user permission for OAuth, identify relying party and its permissions scope
* For OAuth, use a secure Android method to deliver access tokens

## Best Practices
* [Best Practices for Performance](http://developer.android.com/training/best-performance.html)
* [Best Practices for User Interface](https://developer.android.com/training/best-ui.html)
* [Best Practices for Interaction and Engagement](http://developer.android.com/training/best-ux.html)

## Future Revisions
While endeavouring to produce an initial version of the style guide it’s fully expected this will be require further revisions going forward.

